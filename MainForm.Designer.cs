﻿namespace InventoryApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemListBox = new System.Windows.Forms.ListBox();
            this.categoryLabel = new System.Windows.Forms.Label();
            this.categoryComboBox = new System.Windows.Forms.ComboBox();
            this.organicSearchCheckBox = new System.Windows.Forms.CheckBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.viewItemButton = new System.Windows.Forms.Button();
            this.clearSearchButton = new System.Windows.Forms.Button();
            this.restockButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.categoryBox = new System.Windows.Forms.ComboBox();
            this.organicCheckbox = new System.Windows.Forms.CheckBox();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.unitsTextBox = new System.Windows.Forms.TextBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.addItemButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.outOfStockButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // itemListBox
            // 
            this.itemListBox.FormattingEnabled = true;
            this.itemListBox.Location = new System.Drawing.Point(41, 42);
            this.itemListBox.Name = "itemListBox";
            this.itemListBox.Size = new System.Drawing.Size(444, 316);
            this.itemListBox.TabIndex = 0;
            // 
            // categoryLabel
            // 
            this.categoryLabel.AutoSize = true;
            this.categoryLabel.Location = new System.Drawing.Point(38, 15);
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size(52, 13);
            this.categoryLabel.TabIndex = 2;
            this.categoryLabel.Text = "Category:";
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.FormattingEnabled = true;
            this.categoryComboBox.Location = new System.Drawing.Point(96, 12);
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size(121, 21);
            this.categoryComboBox.TabIndex = 3;
            // 
            // organicSearchCheckBox
            // 
            this.organicSearchCheckBox.AutoSize = true;
            this.organicSearchCheckBox.Location = new System.Drawing.Point(223, 14);
            this.organicSearchCheckBox.Name = "organicSearchCheckBox";
            this.organicSearchCheckBox.Size = new System.Drawing.Size(63, 17);
            this.organicSearchCheckBox.TabIndex = 5;
            this.organicSearchCheckBox.Text = "Organic";
            this.organicSearchCheckBox.UseVisualStyleBackColor = true;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(292, 10);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 6;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // viewItemButton
            // 
            this.viewItemButton.Location = new System.Drawing.Point(41, 364);
            this.viewItemButton.Name = "viewItemButton";
            this.viewItemButton.Size = new System.Drawing.Size(136, 23);
            this.viewItemButton.TabIndex = 7;
            this.viewItemButton.Text = "View Selected Item";
            this.viewItemButton.UseVisualStyleBackColor = true;
            this.viewItemButton.Click += new System.EventHandler(this.viewItemButton_Click);
            // 
            // clearSearchButton
            // 
            this.clearSearchButton.Location = new System.Drawing.Point(381, 10);
            this.clearSearchButton.Name = "clearSearchButton";
            this.clearSearchButton.Size = new System.Drawing.Size(104, 23);
            this.clearSearchButton.TabIndex = 8;
            this.clearSearchButton.Text = "Clear Search";
            this.clearSearchButton.UseVisualStyleBackColor = true;
            this.clearSearchButton.Click += new System.EventHandler(this.clearSearchButton_Click);
            // 
            // restockButton
            // 
            this.restockButton.Location = new System.Drawing.Point(205, 364);
            this.restockButton.Name = "restockButton";
            this.restockButton.Size = new System.Drawing.Size(138, 23);
            this.restockButton.TabIndex = 9;
            this.restockButton.Text = "Restock Selected";
            this.restockButton.UseVisualStyleBackColor = true;
            this.restockButton.Click += new System.EventHandler(this.restockButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 423);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Add New Item";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(41, 439);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTextBox.TabIndex = 11;
            this.nameTextBox.Text = "Name";
            // 
            // categoryBox
            // 
            this.categoryBox.FormattingEnabled = true;
            this.categoryBox.Location = new System.Drawing.Point(161, 439);
            this.categoryBox.Name = "categoryBox";
            this.categoryBox.Size = new System.Drawing.Size(100, 21);
            this.categoryBox.TabIndex = 12;
            this.categoryBox.Text = "Category";
            // 
            // organicCheckbox
            // 
            this.organicCheckbox.AutoSize = true;
            this.organicCheckbox.Location = new System.Drawing.Point(280, 441);
            this.organicCheckbox.Name = "organicCheckbox";
            this.organicCheckbox.Size = new System.Drawing.Size(63, 17);
            this.organicCheckbox.TabIndex = 13;
            this.organicCheckbox.Text = "Organic";
            this.organicCheckbox.UseVisualStyleBackColor = true;
            // 
            // amountTextBox
            // 
            this.amountTextBox.Location = new System.Drawing.Point(41, 466);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.Size = new System.Drawing.Size(100, 20);
            this.amountTextBox.TabIndex = 14;
            this.amountTextBox.Text = "Amount";
            // 
            // unitsTextBox
            // 
            this.unitsTextBox.Location = new System.Drawing.Point(161, 466);
            this.unitsTextBox.Name = "unitsTextBox";
            this.unitsTextBox.Size = new System.Drawing.Size(100, 20);
            this.unitsTextBox.TabIndex = 15;
            this.unitsTextBox.Text = "Unit";
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(280, 466);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(100, 20);
            this.priceTextBox.TabIndex = 16;
            this.priceTextBox.Text = "Price";
            // 
            // addItemButton
            // 
            this.addItemButton.Location = new System.Drawing.Point(410, 451);
            this.addItemButton.Name = "addItemButton";
            this.addItemButton.Size = new System.Drawing.Size(75, 23);
            this.addItemButton.TabIndex = 17;
            this.addItemButton.Text = "Add Item";
            this.addItemButton.UseVisualStyleBackColor = true;
            this.addItemButton.Click += new System.EventHandler(this.addItemButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(368, 364);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(117, 23);
            this.deleteButton.TabIndex = 18;
            this.deleteButton.Text = "Delete Selected Item";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // outOfStockButton
            // 
            this.outOfStockButton.Location = new System.Drawing.Point(41, 394);
            this.outOfStockButton.Name = "outOfStockButton";
            this.outOfStockButton.Size = new System.Drawing.Size(136, 23);
            this.outOfStockButton.TabIndex = 19;
            this.outOfStockButton.Text = "Show All Out of Stock";
            this.outOfStockButton.UseVisualStyleBackColor = true;
            this.outOfStockButton.Click += new System.EventHandler(this.outOfStockButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 510);
            this.Controls.Add(this.outOfStockButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addItemButton);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(this.unitsTextBox);
            this.Controls.Add(this.amountTextBox);
            this.Controls.Add(this.organicCheckbox);
            this.Controls.Add(this.categoryBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.restockButton);
            this.Controls.Add(this.clearSearchButton);
            this.Controls.Add(this.viewItemButton);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.organicSearchCheckBox);
            this.Controls.Add(this.categoryComboBox);
            this.Controls.Add(this.categoryLabel);
            this.Controls.Add(this.itemListBox);
            this.Name = "MainForm";
            this.Text = "InventoryApp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox itemListBox;
        private System.Windows.Forms.Label categoryLabel;
        private System.Windows.Forms.ComboBox categoryComboBox;
        private System.Windows.Forms.CheckBox organicSearchCheckBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button viewItemButton;
        private System.Windows.Forms.Button clearSearchButton;
        private System.Windows.Forms.Button restockButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.ComboBox categoryBox;
        private System.Windows.Forms.CheckBox organicCheckbox;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.TextBox unitsTextBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.Button addItemButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button outOfStockButton;
    }
}

