﻿//Created by Calista Cartwright on February 23rd 2020

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace InventoryApp
{
    class InventoryItemManager
    {
        private List<InventoryItem> items = new List<InventoryItem>();
        private int idCounter = 0;
        public int getNewId() {
            idCounter++;
            return idCounter;
        }

        public void AddItem(InventoryItem item)
        {
            //Add item
            items.Add(item);
        }


        public void RemoveItem(int id)
        {
            //Remove item
            for (int i = 0; i < items.Count; i++) {
                if (id == items[i].getId()) {
                    InventoryItem item = GetItem(i);
                    items.Remove(item);
                }
            }
            
        }


        public void RestockItem(int id, int newAmt)
        {
            //Adds given amt to current amt of item
            InventoryItem item = getItemById(id);
            item.setAmt(item.getAmt() + newAmt);
        }

        public InventoryItem getItemById(int id)
        {
            for (int i = 0; i < items.Count; i++) {
                if (items[i].getId() == id) {
                    return items[i];
                }
            }
            return null;
        }

        //Get item
        public InventoryItem GetItem(int index)
        {
            InventoryItem item = items[index];
            return item;
        }

        public List<InventoryItem> GetItems()
        {           
            return items;
        }



        public void EditItem(int index, string name, string category, int amt, string units, double price, bool organic)
        {
            //Takes params from form and allow user to update the information
            //of an item at the given index
            items[index].setName(name);
            items[index].setCategory(category);
            items[index].setOrganic(organic);
            items[index].setUnits(units);
            items[index].setPrice(price);
            items[index].setAmt(amt);
            if (amt <= 0)
            {
                items[index].setOutOfStock(true);
            }
            else
            {
                items[index].setOutOfStock(false);
            }
        }

        public List<InventoryItem> Search(bool organic, string category)
        {
            List<InventoryItem> results = new List<InventoryItem>();
            //Checks to see if an items category and organic bool matches with
            //search parameters

            bool passOrganic;
            bool passCategory;
            for (int i = 0; i < items.Count(); i++)
            {
                passCategory = false;
                passOrganic = false;
                if (organic == items[i].getOrganic())
                {
                    passOrganic = true;
                }
                if (category == items[i].getCategory())
                {
                    passCategory = true;  
                }

                if (passCategory && passOrganic) {
                    results.Add(items[i]);
                }
            }
            return results;
        }
    }

}
