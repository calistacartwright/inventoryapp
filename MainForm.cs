﻿//Created by Calista Cartwright on February 27th 2020
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace InventoryApp
{
    public partial class MainForm : Form
    {

        //Variables
        private InventoryItem item;
        private List<InventoryItem> items;
        private InventoryItemManager manager = new InventoryItemManager();
        private int selectedIndex;
        private string[] categories = { "Meat", "Baking", "Vegetable", "Seafood" };

        public MainForm()
        {
            InitializeComponent();

            //Create initial items
            manager.AddItem(new InventoryItem("Beef", "Meat", "pounds", true, 18, 10.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Beef", "Meat", "pounds", false, 0, 5.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Chicken", "Meat", "pounds", true, 32, 4.49, manager.getNewId()));
            manager.AddItem(new InventoryItem("Chicken", "Meat", "pounds", false, 56, 2.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Pork", "Meat", "pounds", true, 14, 5.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Pork", "Meat", "pounds", false, 0, 3.49, manager.getNewId()));
            manager.AddItem(new InventoryItem("Carrot", "Vegetable", "pounds", true, 18, .99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Carrot", "Vegetable", "pounds", false, 43, .49, manager.getNewId()));
            manager.AddItem(new InventoryItem("Broccoli", "Vegetable", "pounds", true, 32, 2.29, manager.getNewId()));
            manager.AddItem(new InventoryItem("Broccoli", "Vegetable", "pounds", false, 56, 1.79, manager.getNewId()));
            manager.AddItem(new InventoryItem("Onion", "Vegetable", "pounds", true, 14, 1.49, manager.getNewId()));
            manager.AddItem(new InventoryItem("Onion", "Vegetable", "pounds", false, 26, .79, manager.getNewId()));
            manager.AddItem(new InventoryItem("Flour", "Baking", "pounds", false, 18, 2.49, manager.getNewId()));
            manager.AddItem(new InventoryItem("Sugar", "Baking", "pounds", false, 43, 1.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Cane Sugar", "Baking", "pounds", true, 32, 3.49, manager.getNewId()));
            manager.AddItem(new InventoryItem("Chocolate", "Baking", "bars", false, 56, 1.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Eggs", "Baking", "eggs", true, 14, .19, manager.getNewId()));
            manager.AddItem(new InventoryItem("Milk", "Baking", "gallons", true, 26, 2.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Salmon", "Seafood", "pounds", true, 0, 12.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Salmon", "Seafood", "pounds", false, 0, 9.49, manager.getNewId()));
            manager.AddItem(new InventoryItem("Tuna", "Seafood", "pounds", true, 32, 14.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Tuna", "Seafood", "pounds", false, 56, 12.99, manager.getNewId()));
            manager.AddItem(new InventoryItem("Shrimp", "Seafood", "pounds", true, 14, 6.49, manager.getNewId()));
            manager.AddItem(new InventoryItem("Shrimp", "Seafood", "pounds", false, 0, 4.99, manager.getNewId()));

            items = manager.GetItems();

            //Set listbox
            for (int i = 0; i < manager.GetItems().Count(); i++) {
                itemListBox.Items.Add(manager.GetItem(i).getListString());
            }

            //Sets values in dropdowns
            for (int i = 0; i < categories.Length; i++)
            {
                categoryComboBox.Items.Add(categories[i]);
                categoryBox.Items.Add(categories[i]);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            //Prevent crashing if no category selected
            try
            {
                items = manager.Search(organicSearchCheckBox.Checked, categoryComboBox.SelectedItem.ToString());
                
                //Set list with search results
                itemListBox.Items.Clear();
                for (int i = 0; i < items.Count; i++)
                {
                    itemListBox.Items.Add(items[i].getListString());
                }
            }
            catch {
                MessageBox.Show("Make sure a category is selected when searching");
            }

                    
            
        }

        private void viewItemButton_Click(object sender, EventArgs e)
        {
            //Prevent crash from no item selected
            try
            {
                //Opens a new form displaying items properties
                selectedIndex = itemListBox.SelectedIndex;
                ViewItemForm viewForm = new ViewItemForm();

                item = items[selectedIndex];
                string nameLabel = item.getName();
                string categoryLabel = item.getCategory();
                string amountLabel = item.getAmt().ToString() + " " + item.getUnits();
                string organicLabel = item.getOrganic().ToString();
                string priceLabel = "$" + item.getPrice().ToString();

                viewForm.nameLabel.Text = nameLabel;
                viewForm.categoryLabel.Text = categoryLabel;
                viewForm.amountLabel.Text = amountLabel;
                viewForm.organicLabel.Text = organicLabel;
                viewForm.priceLabel.Text = priceLabel;
                viewForm.ShowDialog();
            }
            catch {
                MessageBox.Show("Make sure an item is selected");
            }

        }

        private void clearSearchButton_Click(object sender, EventArgs e)
        {
            //Clears search results
            itemListBox.Items.Clear();
            for (int i = 0; i < manager.GetItems().Count(); i++)
            {
                itemListBox.Items.Add(manager.GetItem(i).getListString());
            }
            items = manager.GetItems();
        }

        private void restockButton_Click(object sender, EventArgs e)
        {
            //Prevent crash from no selected item
            try
            {
                //Add 5 to item's amount through Restock()
                selectedIndex = itemListBox.SelectedIndex;
                InventoryItem currentItem = manager.getItemById(items[selectedIndex].getId());
                manager.RestockItem(currentItem.getId(), 5);
                itemListBox.Items.Clear();
                for (int i = 0; i < /*manager.GetItems().Count()*/items.Count(); i++)
                {
                    //itemListBox.Items.Add(manager.GetItem(i).getListString());
                    itemListBox.Items.Add(items[i].getListString());
                }
            }
            catch {
                MessageBox.Show("Make sure an item is selected");
            }
        }

        private void addItemButton_Click(object sender, EventArgs e)
        {
            //Check for errors in form using try-catch.
            try
            {
                //Certain inputs are allowed in the application to create an inventory item object.
                string name = nameTextBox.Text;
                string category = categoryBox.SelectedItem.ToString();
                string units = unitsTextBox.Text;
                bool organic = organicCheckbox.Checked;
                int amt = int.Parse(amountTextBox.Text);
                double price = double.Parse(priceTextBox.Text);

                //Adds item to list if no errors occured.
                manager.AddItem(new InventoryItem(name, category, units, organic, amt, price, manager.getNewId()));


                //Clear all input after item is created.
                nameTextBox.Text = "Name";
                categoryBox.Text = "Category";
                unitsTextBox.Text = "Unit";
                amountTextBox.Text = "Amount";
                priceTextBox.Text = "Price";
                organicCheckbox.Checked = false;
                
                itemListBox.Items.Clear();
                for (int i = 0; i < manager.GetItems().Count(); i++)
                {
                    itemListBox.Items.Add(manager.GetItem(i).getListString());
                }
            }
            catch
            {

                MessageBox.Show("Invalid input(s)!");
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            //Prevents crash from no selected item
            try
            {
                //Delete item from manager using id
                selectedIndex = itemListBox.SelectedIndex;
                manager.RemoveItem(items[selectedIndex].getId());
                itemListBox.Items.Clear();
                for (int i = 0; i < manager.GetItems().Count(); i++)
                {
                    itemListBox.Items.Add(manager.GetItem(i).getListString());
                }
            }
            catch {
                MessageBox.Show("Make sure an item is selected");
            }
        }

        private void outOfStockButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<InventoryItem> outOfStockList = new List<InventoryItem>();
                for (int i = 0; i < manager.GetItems().Count(); i++)
                {
                    if (manager.GetItem(i).getAmt() == 0)
                    {
                        outOfStockList.Add(manager.GetItem(i));
                    }
                }

                items = outOfStockList;
                itemListBox.Items.Clear();
                for (int i = 0; i < outOfStockList.Count(); i++)
                {
                    itemListBox.Items.Add(outOfStockList[i].getListString());
                }
            }
            catch {
                MessageBox.Show("Invalid input");
            }

        }
    }
}
