﻿//Created by Calista Cartwright on February 8th 2020
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp
{
    //Create an InventoryItem class.
    class InventoryItem
    {
        private string name;
        private string category;
        private string units;
        private bool organic;
        private bool outOfStock;
        private int amt;
        private double price;
        private int id;
        private string listString;

        public InventoryItem(string name, string category, string units, bool organic, int amt, double price, int id)
        {
            this.name = name;
            this.category = category;
            this.units = units;
            this.organic = organic;
            this.amt = amt;
            this.price = price;
            this.id = id;

            //OutOfStock set in relation to the given amount during construction.
            if (this.amt > 0)
            {
                outOfStock = false;
            }
            else
            {
                outOfStock = true;
            }

            if (organic)
            {
                this.listString = id.ToString() + ". " + name + ", Organic, Amount: " + amt.ToString();
            }
            else {
                this.listString = id.ToString() + ". " + name + ", Amount: " + amt.ToString();
            }
        }

        //Getters
        public string getName()
        {
            return name;
        }

        public string getCategory()
        {
            return category;
        }

        public string getUnits()
        {
            return units;
        }

        public bool getOrganic()
        {
            return organic;
        }

        public int getAmt()
        {
            return amt;
        }

        public int getId()
        {
            return id;
        }

        public bool getOutOfStock()
        {
            return outOfStock;
        }

        public double getPrice()
        {
            return price;
        }

        public string getListString() {

            if (organic)
            {
                listString = id.ToString() + ". " + name + ", Organic, Amount: " + amt.ToString();
            }
            else
            {
                listString = id.ToString() + ". " + name + ", Amount: " + amt.ToString();
            }
            return listString;
        }

        //Setters
        public void setName(string newName)
        {
            name = newName;
        }

        public void setCategory(string newCategory)
        {
            category = newCategory;
        }

        public void setUnits(string newUnits)
        {
            units = newUnits;
        }

        public void setOrganic(bool newOrganic)
        {
            organic = newOrganic;
        }

        public void setAmt(int newAmt)
        {
            amt = newAmt;
        }

        public void setPrice(double newPrice)
        {
            price = newPrice;
        }

        public void setOutOfStock(bool newOutOfStock)
        {
            outOfStock = newOutOfStock;
        }
    }
}
